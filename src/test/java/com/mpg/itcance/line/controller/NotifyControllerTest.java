package com.mpg.itcance.line.controller;

import java.io.File;
import java.io.FileInputStream;

import javax.transaction.Transactional;

import com.mpg.itcance.line.entity.Parameter;
import com.mpg.itcance.line.entity.ParameterDetail;
import com.mpg.itcance.line.repository.ParameterDetailRepository;
import com.mpg.itcance.line.repository.ParameterRepository;
import com.mpg.itcance.line.util.CommonUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class NotifyControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterDetailRepository parameterDetailRepository;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void sendSimpleMessageLineNotify() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessage/simple").param("message", "Send form mockMvc"))
                .andExpect(status().isOk()).andExpect(content().json(
                        "{\"success\": true,\"message\": \"Send notify case default successful.\",\"data\": {} }"));
    }

    @Test
    public void sendSimpleMessageLineNotify_special_character_content() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessage/simple").param("message",
                "11234567890-=+_)P(O*&^%$RE#@W!()[]\\//\\//  \" \'  ")).andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": true,\"message\": \"Send notify case default successful.\",\"data\": {} }"));
    }

    @Test
    public void sendSimpleMessageLineNotify_not_found_template() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessage/simple-xxx").param("message", "Send form mockMvc"))
                .andExpect(status().isOk()).andExpect(content().json(
                        "{\"success\": false,\"message\": \"Not found Template [Template Code : simple-xxx]\",\"data\": {},\"error_code\":\"LIN_ERROR_1001\"}"));
    }

    @Test
    public void sendSimpleMessageLineNotify_not_found_message() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessage/simple")).andExpect(status().isOk()).andExpect(content().json(
                "{\"success\": true,\"message\": \"Send notify case default successful.\",\"data\": {} }"));

    }

    @Test
    public void sendNotifyLineMessageByProviderCode() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByProviderCode/simple").param("message", "Send form mockMvc")
                .param("code", "digital001")).andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": true,\"message\": \"Send notify case send by provider code successful.\",\"data\": {} }"));

    }

    @Test
    public void sendNotifyLineMessageByProviderCode_not_found_provider() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByProviderCode/alert").param("message", "Send form mockMvc")
                .param("code", "digital001-test_xxx")).andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Not found line properties config.\",\"data\": {},\"error_code\":\"LIN_ERROR_1002\"}"));
    }

    @Test
    public void sendNotifyLineMessageByProviderCode_provider_empty() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByProviderCode/alert").param("message", "Send form mockMvc"))
                .andExpect(status().isOk()).andExpect(content().json(
                        "{\"success\": false,\"message\": \"Provider has require to send.\",\"data\": {},\"error_code\":\"LIN_ERROR_1002\"}"));
    }

    @Test
    public void sendNotifyLineMessageByToken() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByToken/simple").param("message", "Send form mockMvc")
                .param("lineToken", "pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp")).andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": true,\"message\": \"Send notify case send by token successful.\",\"data\": {} }"));
    }

    @Test
    public void sendNotifyLineMessageByToken_not_found_token() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByToken/alert").param("message", "Send form mockMvc")
                .param("lineToken", "xxxxxx")).andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": false,\"message\": \"Not found token line.\",\"data\": {},\"error_code\":\"LIN_ERROR_1003\"}"));
    }

    @Test
    public void sendNotifyLineMessageByProviderCode_token_empty() throws Exception {
        mockMvc.perform(post("/notify/notifyLineMessageByToken/alert").param("message", "Send form mockMvc"))
                .andExpect(status().isOk()).andExpect(content().json(
                        "{\"success\": false,\"message\": \"Token has require to send.\",\"data\": {},\"error_code\":\"LIN_ERROR_1003\"}"));
    }

}
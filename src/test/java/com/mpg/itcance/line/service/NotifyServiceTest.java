package com.mpg.itcance.line.service;

import javax.transaction.Transactional;

import com.mpg.itcance.line.entity.Parameter;
import com.mpg.itcance.line.entity.ParameterDetail;
import com.mpg.itcance.line.model.ResponseModel;
import com.mpg.itcance.line.repository.ParameterDetailRepository;
import com.mpg.itcance.line.repository.ParameterRepository;
import com.mpg.itcance.line.util.CommonUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;


import org.junit.Assert;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class NotifyServiceTest {

    @Autowired
    NotifyService notifyService;

    @Autowired
    ParameterDetailRepository parameterDetailRepository;

    @Autowired
    ParameterRepository parameterRepository;


    @Test
    public void case_simple_notify() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("template", "simple");

        result = notifyService.notifyLineMessage(mapParam, result, mapParam.get("template"));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    public void case_simple_notify_case_not_found_template() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("template", "xxxxxx");

        result = notifyService.notifyLineMessage(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_provider() {

        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("code", "digital002");
        mapParam.put("template", "information");

        result = notifyService.notifyLineMessageByProviderCode(mapParam, result, mapParam.get("template"));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_provider_not_found_provider() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("code", "digitalxxx");
        mapParam.put("template", "simple");

        result = notifyService.notifyLineMessageByProviderCode(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_provider_not_found_template() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("code", "digital001");
        mapParam.put("template", "xxx123");

        result = notifyService.notifyLineMessageByProviderCode(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_provider_have_provider_but_not_have_token() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("code", "digital003");
        mapParam.put("template", "simple-test-template");

        result = notifyService.notifyLineMessageByProviderCode(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_token() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("lineToken", "pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp");
        mapParam.put("template", "information");

        result = notifyService.notifyLineMessageByToken(mapParam, result, mapParam.get("template"));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_token_not_found_template() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("lineToken", "pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp");
        mapParam.put("template", "1112233");

        result = notifyService.notifyLineMessageByToken(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Test
    public void case_send_noti_by_token_not_found_token() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "Send from Junit Test");
        mapParam.put("lineToken", "1234565fds1f5sdf4sdf4");
        mapParam.put("template", "information");

        result = notifyService.notifyLineMessageByToken(mapParam, result, mapParam.get("template"));
        Assert.assertFalse(result.getSuccess());
    }

    @Transactional
    @Test
    public void case_send_msg_with_special_character() {
        ResponseModel result = new ResponseModel();
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("message", "11234567890-=+_)P(O*&^%$RE#@W!()[]\\//\\//  \" \'  ");
        mapParam.put("lineToken", "pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp");
        mapParam.put("template", "information");

        result = notifyService.notifyLineMessageByToken(mapParam, result, mapParam.get("template"));
        Assert.assertTrue(result.getSuccess());
    }

}
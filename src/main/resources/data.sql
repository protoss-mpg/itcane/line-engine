insert into parameter(id,code,description,flag_active) values(1000,'LIN001','กำหนดค่า Configure Line Gateway',1)
insert into parameter(id,code,description,flag_active) values(2000,'LIN002','กำหนด Template Line Notify Message',1)

-- insert into parameter_detail(id,code,create_date,variable2,variable4,flag_active,parameter) values(1001,'digital001','2019-10-02 00:00:00','KMGB7rBMQUB2KrEW4SLW214lRcFQiLSFnohX5o53DlO','Y',1,1000);
insert into parameter_detail(id,code,create_date,variable2,variable4,flag_active,parameter) values(1001,'digital001','2019-10-02 00:00:00','pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp','Y',1,1000)
insert into parameter_detail(id,code,create_date,variable2,variable4,flag_active,parameter) values(1002,'digital002','2019-10-02 00:00:00','pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp','Y',1,1000)
insert into parameter_detail(id,code,create_date,variable2,variable4,flag_active,parameter) values(1003,'digital003','2019-10-02 00:00:00','pRb4frBr5BmTrzYa5dhGTZWsanXfiWntfL17hQiPikp','Y',1,1000)
insert into parameter_detail(id,code,create_date,variable1,flag_active,parameter) values(2001,'alert','2019-10-02 00:00:00','[...alert template...] Msg : $message',1,2000)
insert into parameter_detail(id,code,create_date,variable1,flag_active,parameter) values(2002,'information','2019-10-02 00:00:00','[...information template...] Msg : $message',1,2000)
insert into parameter_detail(id,code,create_date,variable1,flag_active,parameter) values(2003,'simple','2019-10-02 00:00:00','[...simple template...] Msg : $message',1,2000)


package com.mpg.itcance.line;

import java.util.HashMap;
import java.util.Map;

public class ApplicationConstant {

	private ApplicationConstant(){

	}
	public static final String ERROR_CODE_1000 ="LIN_ERROR_1000";
	public static final String ERROR_CODE_1001 ="LIN_ERROR_1001";
	public static final String ERROR_CODE_1002 ="LIN_ERROR_1002";
	public static final String ERROR_CODE_1003 ="LIN_ERROR_1003";
	public static final String CONTENT_TYPE_JSON ="application/json; charset=utf-8";
	public static final String CONTENT_TYPE_KEY ="application/json; charset=utf-8";
	public static final boolean RESPONSE_STATUS_SUCCESS =true;
	public static final boolean RESPONSE_STATUS_ERROR =false;
	public static final Map<String,String> MAP_CONFIG_LINE = new HashMap<>();
	static {
		MAP_CONFIG_LINE.put("CODE_CONFIG", "LIN001");
		MAP_CONFIG_LINE.put("CODE_TEMPLATE_MSG", "LIN002");
	}

}

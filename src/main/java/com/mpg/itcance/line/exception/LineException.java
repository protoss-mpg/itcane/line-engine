package com.mpg.itcance.line.exception;

public class LineException extends RuntimeException {

    public LineException(){

    }

    public LineException(String errorMessage) {
        super(errorMessage);

    }
}

package com.mpg.itcance.line.repository;

import java.util.List;
import com.mpg.itcance.line.entity.ParameterDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ParameterDetailRepository extends JpaSpecificationExecutor<ParameterDetail>, JpaRepository<ParameterDetail, Long>{
    
        @Query("select DISTINCT pd from ParameterDetail pd " +
            "left join pd.parameter p " +
            "where p.code in :parameterCode " +
            "and pd.code = :code " +
            "and pd.flagActive = true " +
            "order by pd.id ")
        List<ParameterDetail> findByParameterCodeInAndParameterDetailCodeIsActive( @Param("parameterCode") String parameterCode,
                                                                                   @Param("code")String code
                                                                                );

        @Query("select DISTINCT pd from ParameterDetail pd " +
            "left join pd.parameter p " +
            "where p.code in :parameterCode " +
            "and pd.variable2 = :variable2 " +
            "and pd.flagActive = true " +
            "order by pd.id ")
        List<ParameterDetail> findByParameterCodeInAndVariable2IsActive(@Param("parameterCode") String parameterCode,
                                                                        @Param("variable2")String variable2
                                                                    );

        @Query("select DISTINCT pd from ParameterDetail pd " +
            "left join pd.parameter p " +
            "where p.code = :parameterCode " +
            "and pd.variable4 = 'Y' " +
            "and pd.flagActive = true " +
            "order by pd.id ")
        List<ParameterDetail> findByParameterCodeInAndVariable4IsActive(@Param("parameterCode") String parameterCode
                                                                    );



    

}
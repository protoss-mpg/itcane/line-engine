package com.mpg.itcance.line.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.mpg.itcance.line.ApplicationConstant;
import com.mpg.itcance.line.model.ResponseModel;
import com.mpg.itcance.line.service.NotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notify")
public class NotifyController {

     @Autowired
     private NotifyService notifyService;

     @PostMapping("/notifyLineMessage/{templateCode}")
     public ResponseEntity<ResponseModel> notifyLineMessage(HttpServletRequest request,@PathVariable String templateCode) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               Map<String, String> mapContext = new HashMap<>();
               Map<String, String[]> paramerers = request.getParameterMap();
               for (Object key : paramerers.keySet()) {
                    String value = paramerers.get(key)[0];
                    mapContext.put(String.valueOf(key), value);
               }

               result = notifyService.notifyLineMessage(mapContext, result,templateCode);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }

     @PostMapping("/notifyLineMessageByProviderCode/{templateCode}")
     public ResponseEntity<ResponseModel> notifyLineMessageByProviderCode(HttpServletRequest request,@PathVariable String templateCode) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               Map<String, String> mapContext = new HashMap<>();
               Map<String, String[]> paramerers = request.getParameterMap();
               for (Object key : paramerers.keySet()) {
                    String value = paramerers.get(key)[0];
                    mapContext.put(String.valueOf(key), value);
               }

               result = notifyService.notifyLineMessageByProviderCode(mapContext, result,templateCode);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }

     @PostMapping("/notifyLineMessageByToken/{templateCode}")
     public ResponseEntity<ResponseModel> notifyLineMessageByToken(HttpServletRequest request,@PathVariable String templateCode) {
           /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               Map<String, String> mapContext = new HashMap<>();
               Map<String, String[]> paramerers = request.getParameterMap();
               for (Object key : paramerers.keySet()) {
                    String value = paramerers.get(key)[0];

                    mapContext.put(String.valueOf(key), value);
               }

               result = notifyService.notifyLineMessageByToken(mapContext, result,templateCode);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);
          }

     }

}
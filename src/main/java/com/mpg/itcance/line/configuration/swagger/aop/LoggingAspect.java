package com.mpg.itcance.line.configuration.swagger.aop;

import com.mpg.itcance.line.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
public class LoggingAspect {

    @Pointcut("@annotation(com.mpg.itcance.line.configuration.swagger.aop.annotation.LogAccess)")
    public void logAccessMethod() {
        /* add annotation @LogAccess to access  log */
    }

    @Around("logAccessMethod()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
            long start = System.currentTimeMillis();
            Object output = pjp.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info(pjp.getSignature().getName()+"|"+ CommonUtil.getCurrentDateTimestamp()+"|"+elapsedTime);
           
            return output;
    }

}


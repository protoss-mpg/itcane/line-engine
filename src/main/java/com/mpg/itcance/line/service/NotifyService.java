package com.mpg.itcance.line.service;

import java.util.Map;

import com.mpg.itcance.line.model.ResponseModel;

public interface NotifyService{

    ResponseModel notifyLineMessage(Map mapParam,ResponseModel result, String templateCode);
    ResponseModel notifyLineMessageByProviderCode(Map mapParam, ResponseModel result, String templateCode);
    ResponseModel notifyLineMessageByToken(Map mapParam, ResponseModel result, String templateCode);

}
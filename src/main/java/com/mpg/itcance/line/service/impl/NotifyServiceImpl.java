package com.mpg.itcance.line.service.impl;

import java.util.List;
import java.util.Map;
import java.io.StringReader;
import java.io.StringWriter;

import com.mpg.itcance.line.ApplicationConstant;
import com.mpg.itcance.line.entity.ParameterDetail;
import com.mpg.itcance.line.exception.LineException;
import com.mpg.itcance.line.model.ResponseModel;
import com.mpg.itcance.line.repository.ParameterDetailRepository;
import com.mpg.itcance.line.service.LineNotifyServerEngine;
import com.mpg.itcance.line.service.NotifyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;

@Slf4j
@Service
public class NotifyServiceImpl extends LineNotifyServerEngine implements NotifyService {

    @Autowired
    ParameterDetailRepository parameterDetailRepository;


    private Template template;

    @Override
    public ResponseModel notifyLineMessage(Map mapParam, ResponseModel responseModel, String templateCode) {

        String token = null;
        try {
            String messageSend = "";
            /* find line properties casee default */
            List<ParameterDetail> listParamDetail = parameterDetailRepository.findByParameterCodeInAndVariable4IsActive(
                    ApplicationConstant.MAP_CONFIG_LINE.get("CODE_CONFIG"));

             token = listParamDetail.get(0).getVariable2();

            /* find template message with template code */
            List<ParameterDetail> listTemplateLineMessage = parameterDetailRepository
                    .findByParameterCodeInAndParameterDetailCodeIsActive(
                            ApplicationConstant.MAP_CONFIG_LINE.get("CODE_TEMPLATE_MSG"), templateCode);

            if (!listTemplateLineMessage.isEmpty()) {
                String templateMessage = listTemplateLineMessage.get(0).getVariable1();
                /* pass parameter to template message */
                String templateFillter = getContentFromTemplate(templateMessage, mapParam);
                messageSend = templateFillter;

            }else{
                throw new LineException("Not found Template [Template Code : "+templateCode+"]");
            }
            /* send line notify */
           
            ResponseEntity<String> result = sendLineNotify(messageSend, token);

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Send notify case default successful.");
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
    }

    @Override
    public ResponseModel notifyLineMessageByProviderCode(Map mapParam, ResponseModel responseModel, String templateCode) {

        try {
            String messageSend = "";
            String provider = String.valueOf(mapParam.get("code"));
            if(null == provider || "".equals(provider) || "null".equals(provider)){
                throw new LineException("Provider has require to send.");
            }
            String token = null;

            /* find line properties case find by provider code */
            List<ParameterDetail> listParameterDetail = parameterDetailRepository
                    .findByParameterCodeInAndParameterDetailCodeIsActive(
                            ApplicationConstant.MAP_CONFIG_LINE.get("CODE_CONFIG"), provider);

            /* validate properties */
            if (!listParameterDetail.isEmpty()) {
                if (listParameterDetail.get(0).getVariable2() != null) {
                    token = listParameterDetail.get(0).getVariable2();
                } else {
                    throw new LineException(".Not found token in properties.");
                }
            } else {
                throw new LineException("Not found line properties config.");
            }

            /* find template message with template code */
            List<ParameterDetail> listTemplateLineMessage = parameterDetailRepository
                    .findByParameterCodeInAndParameterDetailCodeIsActive(
                            ApplicationConstant.MAP_CONFIG_LINE.get("CODE_TEMPLATE_MSG"), templateCode);

            if (!listTemplateLineMessage.isEmpty()) {
                String templateMessage = listTemplateLineMessage.get(0).getVariable1();
                /* pass parameter to template message */
                templateMessage = getContentFromTemplate(templateMessage, mapParam);
                messageSend = templateMessage;

            }else{
                throw new LineException("Not found Template [Template Code : "+template+"] ");
            }
            /* send line notify */
            ResponseEntity<String> result = sendLineNotify(messageSend, token);

            if (result == null)
                throw new LineException("Can't send line notify.");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Send notify case send by provider code successful.");
            return responseModel;

        } catch (Exception e) {

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1002);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
    }

    @Override
    public ResponseModel notifyLineMessageByToken(Map mapParam, ResponseModel responseModel, String templateCode) {

        try {
            String messageSend = "";
            String token = String.valueOf(mapParam.get("lineToken"));
            if(null == token || "".equals(token) || "null".equals(token)){
                throw new LineException("Token has require to send.");
            }

            /* find line properties case find by token */
            List<ParameterDetail> listParameterDetail = parameterDetailRepository
                    .findByParameterCodeInAndVariable2IsActive(ApplicationConstant.MAP_CONFIG_LINE.get("CODE_CONFIG"),
                            token);

            /* validate properties */
            if (!listParameterDetail.isEmpty()) {

                /* find template message with template code */
                List<ParameterDetail> listTemplateLineMessage = parameterDetailRepository
                        .findByParameterCodeInAndParameterDetailCodeIsActive(
                                ApplicationConstant.MAP_CONFIG_LINE.get("CODE_TEMPLATE_MSG"), templateCode);

                if (!listTemplateLineMessage.isEmpty()) {
                    String templateMessage = listTemplateLineMessage.get(0).getVariable1();
                    /* pass parameter to template message */
                    templateMessage = getContentFromTemplate(templateMessage, mapParam);
                    messageSend = templateMessage;

                }else{
                    throw new LineException("Not found Template [Template Code : "+template+"] ");
                }
                /* send line notify */
                ResponseEntity<String> result = sendLineNotify(messageSend, token);

                if (result == null)
                    throw new LineException("Can't send line notify");

                responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
                responseModel.setError_code(null);
                responseModel.setMessage("Send notify case send by token successful.");
                return responseModel;

            }else{
                throw new LineException("Not found token line.");
            }

        } catch (Exception e) {

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1003);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
    }

    private String getContentFromTemplate(String templateContent, Map mapContext) throws Exception {
         RuntimeServices runtimeServices;
         StringReader stringReader;
         SimpleNode simpleNode;
        if (templateContent != null && templateContent.trim().length() > 0) {
            runtimeServices = RuntimeSingleton.getRuntimeServices();
            stringReader = new StringReader(templateContent);
            simpleNode = runtimeServices.parse(stringReader, "Line Template");

            template = new Template();
            template.setRuntimeServices(runtimeServices);
            template.setData(simpleNode);
            template.initDocument();

            /* add that list to a VelocityContext */
            VelocityContext context = new VelocityContext(mapContext);

            /* get the Template */
            /* now render the template into a Writer */
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            return writer.toString();
        } else {
            return templateContent;
        }

    }
}